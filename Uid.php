<?php

namespace Ksum\Uid;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\ImageManagerStatic as Image;

class Uid{

  public static function getDetails($uid){

    $response = Http::asForm()->post(config('uid-login.server').'/api/users/companydetails',['uid'=>$uid]);

    $data = $response->json();

    if($data["status"]=="200"){

      return $data['result'][0];

    }
    else{
      return false;
    }

  }

  public static function getLogo($userid, $logo){

    if(str_contains($logo, 'api.startupindia.gov.in')){
      $url = $logo;
    }
    else{
      $url = config('uid-login.server').'/uploads/user'.$userid.'/'.$logo;
    }
    $contents = file_get_contents($url);

    return $contents;

  }

  public static function resizeLogo($userid, $logo, $width, $height){

    $content = Uid::getLogo($userid, $logo);

    if($content)
    {
      try{
        $img = Image::make($content);
        
        $img->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        })->resizeCanvas($width, $height, 'center');

        return $img;
      }
      catch(Exception $e){
        return null;
      }
      return null;
    }
    else{
      return null;
    }
 
  }

  public static function saveLogo($userid, $logo, $width, $height, $path){
    $img = Uid::resizeLogo($userid, $logo, $width, $height);
    if($img){
      $resize = $img->encode();

      $image_name = (string) Str::uuid().'.'.Arr::last(explode('.',$logo));

      Storage::put($path.$image_name, $resize);

      return $image_name;
    }
    else{
      return 'no-image.jpg';
    }
    
  }

  public static function checkDipp($dipp){

    $response = Http::asForm()->post(config('uid-login.server').'/api/users/checkuserVerified',['input'=>$dipp]);

    $data = $response->json();

    if($data["status"]=="200"){

      return $data['result'][0]['unique_id'];

    }
    else{
      return false;
    }

  }

}
