<?php

namespace Ksum\Uid;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\ImageManagerStatic as Image;

class UidLogin{

  public $credentials;
  public $data;
  public $details;

  public function __construct($credentials)
  {
    $this->credentials = $credentials;  
  }

  public function login(){

    $response = Http::asForm()->post(config('uid-login.server').'/api/users/login',$this->credentials);

    $data = $response->json();

    if($data["status"]=="200"){

      $this->data = $data['result'];
      return true;
    }
    else{
      return false;
    }

  }

  public function getDetails(){

    $response = Http::asForm()->post(config('uid-login.server').'/api/users/companydetails',['uid'=>$this->data['unique_id']]);

    $data = $response->json();

    if($data["status"]=="200"){

      $this->details = $data['result'][0];

      return true;
    }
    else{
      return false;
    }

  }

  public function getLogo(){

    $url = config('uid-login.server').'/uploads/user'.$this->details['userid'].'/'.$this->details['logo'];
    $contents = file_get_contents($url);

    return $contents;

  }

  public function resizeLogo($width, $height){
    $img = Image::make($this->getLogo());
    
    $img->resize($width, $height, function ($constraint) {
        $constraint->aspectRatio();
    })->resizeCanvas($width, $height, 'center');

    return $img;
  }

  public function saveLogo($width, $height, $path){

    $resize = $this->resizeLogo($width, $height)->encode();

    $image_name = (string) Str::uuid().'.'.Arr::last(explode('.',$this->details['logo']));

    Storage::put($path.$image_name, $resize);

    return $image_name;
  }

}