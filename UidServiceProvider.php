<?php

namespace Ksum\Uid;

use Illuminate\Support\ServiceProvider;

class UidServiceProvider extends ServiceProvider{

    public function boot()
    {
      $this->publishes([
        __DIR__.'/config.php' => config_path('uid-login.php'),
      ]);

      $this->loadViewsFrom(__DIR__.'/views', 'ksum');
      
    }
}